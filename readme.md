# Sistema de Games

Esse projeto é foi desenvolvido utilizando o Framework Laravel 5.8.


# Como rodar o projeto

*OBS: Você deve ter o docker e o docker-compose instalado, para setar o ambiente de desenvolvimento.*

**Agora você deve executar os seguintes comandos para rodar o sistema:**

 1. docker-compose up -d
 2. composer install
 3. cp .env.example .env
 4. php artisan migrate
 5. php artisan key:generate
 6. vendor/bin/phpunit 
 7. php artisan serve
 
 Agora é só abrir o app no navegador, na url: http://localhost:8000

