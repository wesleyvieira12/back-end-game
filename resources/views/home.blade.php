@extends('layouts.app')

@section('content')
    <div class="card-header">Painel</div>

    <div class="card-body">
        <a href="{{route('personas.index')}}" class="btn btn-primary">Personagens</a>
        <a href="{{route('settings.edit')}}" class="btn btn-info">Configurações</a>
    </div>
@endsection
