@extends('layouts.app')

@section('content')
    <div class="card-header">Settings</div>
    <div class="card-body">

        @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('settings.update')}}" class="form" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @if($settings->image_background)
                <img src="{{asset('storage/images/'.$settings->image_background)}}" style="width: 80px"/>
            @endif
            <lable>Imagem de Fundo:</lable>
            <input type="file" name="image_background" value="{{$settings->image_background}}"/><br/><hr/>

            <lable>Texto Principal:</lable>
            <input type="text" name="text_main" value="{{$settings->text_main}}" placeholder="Digite o texto principal"/><br/><hr/>

            <lable>Nome do Jogo:</lable>
            <input type="text" name="game_name" value="{{$settings->game_name}}" placeholder="Digite o nome do jogo"/><br/><hr/>

            <lable>Descrição do Jogo:</lable>
            <input type="text" name="description_game" value="{{$settings->description_game}}" placeholder="Digite uma descrição do jogo"/><br/><hr/>

            <lable>Descrição do Formulário:</lable>
            <input type="text" name="description_form" value="{{$settings->description_form}}" placeholder="Digite uma descrição do formulário"/><br/><hr/>

            <input type="submit" value="Salvar"/>
        </form>
    </div>
@endsection
