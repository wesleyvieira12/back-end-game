@extends('layouts.app')

@section('content')
    <div class="card-header">Editar personagem</div>
    <div class="card-body">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('personas.update',['id' =>$persona->id])}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <img src="{{asset('storage/images/'.$persona->image)}}" style="width: 80px"/>
            <input type="file" name="image" value="{{$persona->image}}"/>
            <input type="text" name="description" value="{{$persona->description}}" placeholder="Digite uma descrição"/>
            <input type="submit" value="Salvar"/>
        </form>
    </div>
@endsection
