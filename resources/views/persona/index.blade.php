@extends('layouts.app')

@section('content')
    <div class="card-header">Personagens</div>
    <div class="card-body">
        @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
<a class="btn btn-success" href="{{route('personas.create')}}">Novo Personagem</a>
<hr>
<div class="row">
<table class="table table-responsive">
    <thead>
        <tr>
            <td>#</td>
            <td>Imagem</td>
            <td>Descrição</td>
            <td>Ações</td>
        </tr>
    </thead>
    <tbody>
        @foreach( $personas as $persona )
            <tr>
                <td>{{$persona->id}}</td>
                <td><img src="{{asset('storage/images/'.$persona->image)}}" style="width: 80px"/></td>
                <td>{{$persona->description}}</td>
                <td><a href="{{route('personas.edit', ['id' => $persona->id])}}" class="btn btn-warning">Editar</a> | <form method="POST" action="{{route('personas.destroy', ['id' => $persona->id])}}">@csrf @method('DELETE')<button type="submit" class="btn btn-danger">Excluir</button></form></td>
            </tr>
        @endforeach
    </tbody>
</table>
{{$personas->links()}}
</div>
</div>
@endsection
