@extends('layouts.app')

@section('content')
    <div class="card-header">Criar novo personagem</div>
    <div class="card-body">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('personas.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="image" value=""/>
            <input type="text" name="description" value="" placeholder="Digite uma descrição"/>
            <input type="submit" value="Salvar"/>
        </form>
    </div>
@endsection
