<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    private $settings = null;

    public function __construct() {
        $this->settings = Setting::first();
        if( $this->settings == null ) {
            $this->settings = Setting::create([
                'image_background' => '',
                'text_main' => '',
                'game_name' => '',
                'description_game' => '',
                'description_form' => '',
            ]);
        }
    }

    public function edit() {
        return view('settings.edit', ['settings' => $this->settings]);
    }

    public function update( Request $request ) {

        if ($request->hasFile('image_background') && $request->file('image_background')->isValid()){

                $nameFile = null;
                $name = uniqid(date('HisYmd'));
                $extension = $request->image_background->extension();
                $nameFile = "{$name}.{$extension}";
                $upload = $request->image_background->storeAs('images', $nameFile);

                if($this->settings->image_background)
                Storage::delete('images/'.$this->settings->image_background);

                $this->settings->image_background = $nameFile;

        } else {
            return redirect()->route('settings.edit')->withErrors('Erro ao salvar a imagem, verifique o tamanho ou extenção do arquivo!');
        }

        $this->settings->text_main = $request->text_main;
        $this->settings->game_name = $request->game_name;
        $this->settings->description_game = $request->description_game;
        $this->settings->description_form = $request->description_form;
        $this->settings->save();

        return redirect()->route('settings.edit')->with('status', 'Configurações atualizadas!');
    }
}
