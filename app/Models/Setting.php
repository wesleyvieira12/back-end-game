<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'image_background',
        'text_main',
        'game_name',
        'description_game',
        'description_form',
    ];
}
