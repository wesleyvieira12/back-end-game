<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PersonaTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testParaVerificarSeEDirecionadoSemEstarLogado()
    {

        $response = $this->get(route('personas.index'));

        $response->assertStatus(302);
    }

    public function testParaVerificarSeAcessaListagemPersonagens()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->get(route('personas.index'));

        $response->assertStatus(200);
    }

    public function testParaVerificarSeAcessaCriacaoPersonagens()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->get(route('personas.create'));

        $response->assertStatus(200);
    }

    public function testParaVerificarSeAcessaEditarPersonagens()
    {
        $user = factory(\App\User::class)->create();
        $persona = factory(\App\Models\Persona::class)->create();
        $response = $this->actingAs($user)->get(route('personas.edit',['id' => $persona->id]));

        $response->assertStatus(200);
    }
}
