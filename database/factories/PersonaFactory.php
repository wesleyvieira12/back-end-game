<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {
    return [
        'image' => $faker->image('public/storage/images',640,480, null, false),
        'description' => $faker->text(200)
    ];
});
